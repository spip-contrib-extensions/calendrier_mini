# Changelog

## 4.1.1 - 2025-02-27

### Fixed

- Chaînes de langue au format SPIP 4.1+

## 4.1.0 - 2024-07-05

### Changed

- Compatible SPIP 4.*

## 4.0.4 - 2023-12-13

### Fixed

- Correction d’un bug JS si pas de date initialisée sur le calendrier mini

## 4.0.3 - 2023-10-26

### Fixed

- Amélioration du filtre `|todate` pour pouvoir convertir un timestamp ou une date dans le format attendu

## 4.0.2 - 2023-10-26

### Fixed

- Correction CSS

## 4.0.0 - 2023-05-18

### Changed

- Version compatible SPIP 4.2
