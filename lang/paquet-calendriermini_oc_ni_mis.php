<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-calendriermini?lang_cible=oc_ni_mis
// ** ne pas modifier le fichier **

return [

	// C
	'calendriermini_description' => '#CALENDRIER_MINI afichà un calendari dau design que ven de dotclear e es pi coumpatible embé l’estile que ven d’aqueu sistèma de blog.
_ Lì soun ajounch d’autre elemen couma balisa, critèri, moudel...',
	'calendriermini_nom' => 'Mini Calendari', # MODIF
	'calendriermini_slogan' => 'Permete l’utilisacioun d’una balisa #CALENDRIER_MINI',
];
