<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-calendriermini?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// C
	'calendriermini_description' => '#CALENDRIER_MINI visualizza un calendario progettato come dotclear, quindi compatibile con gli stili di questo sitema di blog.
_ Altri tool sono aggiunti, come i tag, criteri, modelli...',
	'calendriermini_nom' => 'Mini Calendario',
	'calendriermini_slogan' => 'Consenti l’utilizzo del tag #CALENDRIER_MINI',
];
