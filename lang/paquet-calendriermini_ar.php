<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-calendriermini?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// C
	'calendriermini_description' => '#CALENDRIER_MINI affiche un calendrier au design issu de dotclear et donc compatible avec les styles issus de ce système de blog.
_ Lui sont adjoints d’autre éléments, tels que balises, critères, modèles...',
	'calendriermini_nom' => 'Mini Calendrier', # MODIF
	'calendriermini_slogan' => 'يسمح باستخدام علامة #CALENDRIER_MINI',
];
