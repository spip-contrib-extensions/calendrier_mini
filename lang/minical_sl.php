<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/minical?lang_cible=sl
// ** ne pas modifier le fichier **

return [

	// A
	'aucune_date' => 'Nič v tem mesecu',

	// M
	'mois_precedent' => 'Prejšnji mesec',
	'mois_suivant' => 'Naslednji mesec',
];
