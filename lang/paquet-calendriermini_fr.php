<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/calendrier_mini.git

return [

	// C
	'calendriermini_description' => '#CALENDRIER_MINI affiche un calendrier au design issu de dotclear et donc compatible avec les styles issus de ce système de blog.
_ Lui sont adjoints d’autre éléments, tels que balises, critères, modèles...',
	'calendriermini_nom' => 'Calendrier Mini',
	'calendriermini_slogan' => 'Permet l’utilisation d’une balise #CALENDRIER_MINI',
];
