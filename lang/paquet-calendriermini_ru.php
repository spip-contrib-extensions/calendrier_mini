<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-calendriermini?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// C
	'calendriermini_description' => '#CALENDRIER_MINI отображает календарь в соответствии с дизайном и стилями на сайте.
_Добавлены другие инструменты: теги, критерии, модели...',
	'calendriermini_nom' => 'Мини-Календарь', # MODIF
	'calendriermini_slogan' => 'Позволяет использовать тег #CALENDRIER_MINI',
];
