<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/minical?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// A
	'aucune_date' => 'اين ماه برنامه‌اي نيست',

	// M
	'mois_precedent' => 'ماه قبل',
	'mois_suivant' => 'ماه بعد',
];
