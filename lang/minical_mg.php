<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/minical?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// A
	'aucune_date' => 'Tsy misy na inona na inona amin’ity volana ity',

	// C
	'config_titre_calendriermini' => 'Tetiandro kely',

	// L
	'label_affichage_hors_mois' => 'Andro aranty',
	'label_affichage_hors_mois_0' => 'Tsy aseho ny andro amin’ny volana lasa sy ny ho avy',
	'label_affichage_hors_mois_1' => 'Asehoy ny andro amin’ny volana lasa sy ny ho avy',
	'label_changement_rapide' => 'Fivezivezena',
	'label_changement_rapide_0' => 'Tsy avela hisafidy haingana ny volana sy ny taona',
	'label_changement_rapide_1' => 'Avela hisafidy haingana ny volana sy ny taona',
	'label_format_jour' => 'Fanehoana ny andro',
	'label_format_jour_abbr' => 'Fohy',
	'label_format_jour_initiale' => 'Fanafohezana',
	'label_jour1' => 'Andro voalohan’ny amin’ny herinandro',

	// M
	'mois_precedent' => 'Volana lasa',
	'mois_suivant' => 'Volana ho avy',
];
