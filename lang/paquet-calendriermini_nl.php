<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-calendriermini?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// C
	'calendriermini_description' => '#CALENDRIER_MINI toont een kalender in het dotclear ontwerp en is dus compatibel met de stijl van blogs.
_ Er zijn extra elementen aan toegevoegd, zoals bakens, criteria, modellen...',
	'calendriermini_nom' => 'Mini Kalender',
	'calendriermini_slogan' => 'Maakt het gebruik van #CALENDRIER_MINI mogelijk',
];
