<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-calendriermini?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// C
	'calendriermini_description' => '#CALENDRIER_MINI zobrazí kalendár s dizajnom od dotclear, a preto je kompatibilný so štýlmi tohto  publikačného systému pre blogy.
_ Sám pomáha iným objektom, ako sú tagy, kritériá, šablóny atď.
.',
	'calendriermini_nom' => 'Minikalendár', # MODIF
	'calendriermini_slogan' => 'Umožňuje používať tag #CALENDRIER_MINI',
];
